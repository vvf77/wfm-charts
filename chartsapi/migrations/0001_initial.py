# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Break',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('label', models.CharField(verbose_name='Наименование', max_length=100)),
                ('start', models.DateTimeField(verbose_name='Начало', db_index=True)),
                ('end', models.DateTimeField(verbose_name='Окончание', db_index=True)),
            ],
            options={
                'verbose_name': 'Перерыв',
                'verbose_name_plural': 'Перерывы',
            },
        ),
        migrations.CreateModel(
            name='Department',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('label', models.CharField(verbose_name='Наименование', max_length=100)),
            ],
            options={
                'verbose_name': 'Подразделение',
                'verbose_name_plural': 'Подразделения',
            },
        ),
        migrations.CreateModel(
            name='Organization',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('label', models.CharField(verbose_name='Наименование', max_length=100)),
            ],
            options={
                'verbose_name': 'Организация',
                'verbose_name_plural': 'Организации',
            },
        ),
        migrations.CreateModel(
            name='Shift',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('label', models.CharField(verbose_name='Наименование', max_length=100)),
                ('start', models.DateTimeField(verbose_name='Начало', db_index=True)),
                ('end', models.DateTimeField(verbose_name='Окончание', db_index=True)),
                ('duration', models.IntegerField(verbose_name='Продолжительность')),
            ],
            options={
                'verbose_name': 'Смена',
                'verbose_name_plural': 'Смены',
            },
        ),
        migrations.CreateModel(
            name='Workplace',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('label', models.CharField(verbose_name='Наименование', max_length=100)),
                ('headcount', models.IntegerField(verbose_name='Количество рабочих мест', default=1)),
                ('department', models.ForeignKey(to='chartsapi.Department')),
            ],
            options={
                'verbose_name': 'Помещение',
                'verbose_name_plural': 'Помещения',
            },
        ),
        migrations.AddField(
            model_name='department',
            name='organization',
            field=models.ForeignKey(to='chartsapi.Organization'),
        ),
        migrations.AddField(
            model_name='break',
            name='shift',
            field=models.ForeignKey(to='chartsapi.Shift'),
        ),
    ]
