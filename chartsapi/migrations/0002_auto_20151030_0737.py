# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('chartsapi', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Demand',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('label', models.CharField(verbose_name='Наименование', max_length=100)),
                ('datetime', models.DateTimeField(db_index=True, verbose_name='Когда')),
                ('headcount', models.IntegerField(default=1, verbose_name='Количество рабочих')),
                ('workplace', models.ForeignKey(to='chartsapi.Workplace')),
            ],
        ),
        migrations.CreateModel(
            name='Job',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('label', models.CharField(verbose_name='Наименование', max_length=100)),
            ],
            options={
                'verbose_name': 'Должность',
                'verbose_name_plural': 'Должности',
            },
        ),
        migrations.AddField(
            model_name='shift',
            name='workplace',
            field=models.ForeignKey(default=1, to='chartsapi.Workplace'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='workplace',
            name='job',
            field=models.ForeignKey(default=1, to='chartsapi.Job'),
            preserve_default=False,
        ),
    ]
