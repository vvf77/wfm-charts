# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url

#from . import views

urlpatterns = patterns('',
    url(r'^api/working_times$', 'chartsapi.views.working_times', name='working_times'),
)