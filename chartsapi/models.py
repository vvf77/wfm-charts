from django.db import models


class Organization(models.Model):
    class Meta:
        verbose_name = 'Организация'
        verbose_name_plural = 'Организации'
    label = models.CharField("Наименование", max_length=100)


class Department(models.Model):
    class Meta:
        verbose_name = 'Подразделение'
        verbose_name_plural = 'Подразделения'
    label = models.CharField("Наименование", max_length=100)
    organization = models.ForeignKey(Organization)


class Job(models.Model):
    class Meta:
        verbose_name = 'Должность'
        verbose_name_plural = 'Должности'
    label = models.CharField("Наименование", max_length=100)


class Workplace(models.Model):
    class Meta:
        verbose_name = 'Помещение'
        verbose_name_plural = 'Помещения'
    label = models.CharField("Наименование", max_length=100)
    headcount = models.IntegerField("Количество рабочих мест", default=1)
    department = models.ForeignKey(Department)
    job = models.ForeignKey(Job)


class Shift(models.Model):
    class Meta:
        verbose_name = 'Смена'
        verbose_name_plural = 'Смены'
        ordering = ['start']
    label = models.CharField("Наименование", max_length=100)
    # время кратно 15 минутам
    start = models.DateTimeField("Начало", null=False, blank=False, db_index=True)
    end = models.DateTimeField("Окончание", null=False, blank=False, db_index=True)
    duration = models.IntegerField("Продолжительность")
    workplace = models.ForeignKey(Workplace)


class Break(models.Model):
    class Meta:
        verbose_name = 'Перерыв'
        verbose_name_plural = 'Перерывы'
        ordering = ['start']
    label = models.CharField("Наименование", max_length=100)
    # время кратно 15 минутам
    start = models.DateTimeField("Начало", null=False, blank=False, db_index=True)
    end = models.DateTimeField("Окончание", null=False, blank=False, db_index=True)
    shift = models.ForeignKey(Shift)


class Demand(models.Model):
    class Meta:
        ordering = ['datetime']
    label = models.CharField("Наименование", max_length=100)
    # начало 15-минутного интервала
    datetime = models.DateTimeField("Когда", null=False, blank=False, db_index=True)

    headcount = models.IntegerField("Количество рабочих", default=1)
    workplace = models.ForeignKey(Workplace)

