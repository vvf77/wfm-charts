from datetime import datetime, timedelta
from django.http.response import HttpResponse
from django.shortcuts import render

# Create your views here.
from django.shortcuts import render_to_response
from django.views.decorators.csrf import csrf_exempt
from django.core.context_processors import csrf
from chartsapi.models import Shift, Demand, Workplace
from django.core.serializers.json import DjangoJSONEncoder

import logging
logger = logging.getLogger(__name__)

# TODO: move it to the settings
logger.setLevel(logging.DEBUG)
logger.addHandler(logging.StreamHandler())


def home(request):
    context = {}
    context.update(csrf(request))
    return render_to_response('home.html', context)

def log_ti(ti):
    logger.info('{:%T}-{:%T}'.format(*ti))

def substract_interval(interval1, interval2):
    # B-begin, E-End
    # interval1[0] - B1
    # interval1[1] - E1
    # interval2[0] - B2
    # interval2[1] - E2
    #   /-----\
    # B2 B1 E1 E2
    #    \--/
    # result - empty

    if interval2[0] < interval1[0] and interval1[1] > interval2[1]:
        return []

    # /--\                   /--\
    # B2 E2 B1 E1  or  B1 E1 B2 E2
    #       \--/       \--/
    # result - no changes
    if interval2[1] < interval1[0] or interval2[0] > interval1[1]:
        return [interval1]

    # /------\             /-----\
    # B2 B1 E2 E1  or  B1 B2 E1 E2
    #    \-----/       \-----/
    # result - one shrink interval
    if interval2[0] < interval1[0] < interval2[1]:
        return [(interval2[1], interval1[1])]
    if interval2[0] < interval1[1] < interval2[1]:
        return [(interval2[1], interval1[1])]
    # return two intervals
    return [(interval1[0], interval2[0]), (interval2[1], interval1[1])]


def prepare_shift(sh):
    '''
    :param sh: Shift
    :return: json serializable dict with time intervals
    '''
    # TODO: Навернео лучше будет тут переделать и возвращать примерно в таком же формате как и Demand:
    # TODO:   разбитым по 15-минутным интервалам
    time_intervals = [(sh.start, sh.end)]
    for br in sh.break_set.all():  # for useful "prefetch_related" should get "all" and it is than I need
        new_intervals = []
        for ti in time_intervals:
            new_intervals += substract_interval(ti, (br.start, br.end))
        time_intervals = new_intervals

    return dict(
        id=sh.id,
        duration=sh.duration,  # ??? что это поле означает?
        time_intervals=time_intervals
    )


@csrf_exempt  # TODO: Сделать на клиентской стороне чтобы отправлялся нормально токен и убрать этот декоратор.
def working_times(request):
    '''
    :param request:
    :return: Формирует JSON ответ с данными по заданным фильтрам.
        Фильтры:
            from-to ограничение по времени
        Результат:
            demand - потребность во времени
            planned - распределенное время
    '''
    date_format = '%Y-%m-%d %H:%M:%S'
    default_date_from = datetime.today().strftime('%Y-%m-%d 00:00:00')
    default_date_to = (datetime.today()+timedelta(days=7)).strftime('%Y-%m-%d 00:00:00')
    times_filter = {
        'start__gte': datetime.strptime(request.REQUEST.get('from', default_date_from), date_format),
        'end__lt': datetime.strptime(request.REQUEST.get('to', default_date_to), date_format),
    }
    logger.info('{:%d.%m %T}-{:%d.%m %T}'.format(*times_filter.values()))
    planned = Shift.objects.filter(**times_filter)\
        .prefetch_related('break_set')

    demand = Demand.objects\
        .filter(datetime__gte=times_filter['start__gte'], datetime__lte=times_filter['end__lt'])

    workplaces = Workplace.objects.all()  # TODO: сделать выборку только тех на которые есть ссылки в planned и demand

    result_data = dict(
        planned=[
            prepare_shift(sh) for sh in planned.only('id', 'start', 'duration', 'end', 'workplace_id')
        ],
        demand=list(
            demand.values('id', 'datetime', 'headcount', 'workplace_id')
        ),
        workplaces=list(
            workplaces.values('id', 'label', 'headcount')
        )
    )
    json_result = DjangoJSONEncoder().encode( result_data )
    return HttpResponse(json_result, content_type="application/json")
