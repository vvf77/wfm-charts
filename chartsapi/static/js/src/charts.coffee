
app = angular.module "wfm_charts", ['ui.bootstrap','ui.bootstrap.tpls','n3-line-chart' ]

test_data = (x)->
  x: new Date( new Date().getTime() + x*900000)
  demand: 100*Math.sin x*Math.PI/18
  planned: 100*Math.cos x*Math.PI/18

app.controller 'ChartCtrl', [
  '$scope','commonStorage', 'wfmChartDataService',
  ($scope, commonStorage, dataService)->
    $scope.chart = commonStorage.chart
    $scope.chart_type = 'line'
    window.dbg_cs = commonStorage
    window.dbg_chart_scope = $scope
    $scope.chart.data = dataService.get_chart_data('all')
    unless $scope.chart.data
      $scope.chart.data = (test_data(x) for x in [0..36])
    $scope.data_filters =
      from:new Date "10 29 2015" # (new Date().toDateString())
      to:  new Date "10 29 2015" # (new Date().toDateString())
    $scope.data_filters.to.setDate $scope.data_filters.from.getDate() + 7

    $scope.chart.options.series = [$scope.chart.series_options.demand, $scope.chart.series_options.planned]
    $scope.reload_from_server = ()->
      dataService.reload_from_server($scope.data_filters).then (d)->
        $scope.chart.data = dataService.get_chart_data('all', $scope.data_filters)
        $scope.chart.options.series = [$scope.chart.series_options.demand, $scope.chart.series_options.planned]

    $scope.$watch 'chart_type', ()->
      for chrt in $scope.chart.options.series
        chrt.type = $scope.chart_type
]

tooltip_date_formatter=d3.time.format('%m/%d %H:%M')

param_date_formatter=d3.time.format('%Y-%m-%d %H:%M:%S')

app.value "commonStorage",
  config:
    api_url:'/charts/api/working_times'
  chart:
    options:
      axes:
        x:
          key: 'x'
          type: 'date'
          tooltipFormat:'%d.%m %M:%S'
          ticksFormat:'%H %d.%m'
          ticksRotate:45
          ticks:d3.time.hour
          ticksInterval: 2
          grid:yes
        y:
          type:'linear'
          label:'Сотрудники'
          grid: yes
      series: []
      tooltip:
        mode: 'scrubber',
        formatter: (x,y, series)->
          tooltip_date_formatter(x) + '  .   '+ (Math.round(y*100)/100)
      drawLegend: no
      columnsHGap:1
      drawDots: no

    data: []
    series_options:
      demand:
        y: "demand"
        label: "Demand"
        color: "#5533FF"
        type: "line"
        thickness: '2px'
      planned:
        y: "planned"
        label: "Planned"
        color: "#ff3333"
        type: "line"
        thickness: '2px'

# Сервис для загрузки и обработки данных
class ChartDataService
  chart_types = ['demand', 'planned']
  @$inject = ['$q','$http', 'commonStorage']
  constructor: (@$q, @$http,@commonStorage)->

    @url=@commonStorage.config.api_url
    @cache={}
    @time_index={}
    @current_positions={}
    for ct in chart_types
      @time_index[ct] = {}
      @current_positions[ct] = 0
      @cache[ct] = JSON.parse localStorage.getItem ct


  reload_from_server: (filters=null)->
    self = @
    @$http
      url: @url
      method:'POST'
      responseType:"json"
      xsrfCookieName: "csrftoken"
      xsrfHeaderName: "X-CSRFToken"
      data:filters
    .then (resp)->
      #resp.data
      '''TODO: чтобы не делать много преобразований со времением - можно данные с сервера получать в виде integer=timestamp'''
      for k, data of resp.data
        #self.cache[k] = data
        '''TODO: проверять есть ли уже такой ключ и консалидировать данные с теми что уже есть'''
        localStorage.setItem(k, JSON.stringify(data))
      return resp.data
    , (resp)->
      console.log 'Ajax error', resp

  get_chart_data: (chart_type='demand', filters=null)->
    ''' TODO: Определить в каких еще случаях нужно считать данные из хранилища '''
    if chart_type=='all'
      for ct in chart_types
        unless @cache[ct]
          @cache[ct] = JSON.parse(localStorage.getItem(ct))

    unless @cache[chart_type]
      @cache[chart_type] = JSON.parse(localStorage.getItem(chart_type))

    unless filters
      filters=
        from:new Date (new Date().toDateString())
        to:  new Date (new Date().toDateString())
      filters.to.setDate filters.from.getDate() + 7

    start_time_point_ms = filters.from.getTime()
    count_x_point = (filters.to.getTime() - start_time_point_ms)/900000

    if chart_type == 'all'
      for ct in chart_types
        @current_positions[ct] = 0
    else
      @current_positions[chart_type] = 0

    if chart_type != 'all' and not @cache[chart_type]
      return []

    return (@make_timepoint( new Date(start_time_point_ms+900000*n), chart_type) for n in [0..count_x_point] by 1)

#    @["fill_chart_data_for_#{chart_type}"](filters)

  make_timepoint:(time_point, chart_type)->
    ''' Функция создает точку на графиках. chart_type - тип гарфика или all если оба
      точнее один элемент массива для компоненты вывода графика
    '''
    item =
      x:time_point
    if chart_type == 'all'
      for ct in chart_types
        item[ct] = @make_timepoint(time_point, ct)[ct]
    else
      item[chart_type] = @["chart_point_for_#{chart_type}"](time_point)
    item

  chart_point_for_demand: (time_point)->
    # Функция аггрегирует данные "требование" по одной временной точке
    # Возвращает значение в этой точке
    ''' TODO: формировать и использовать индекс для быстрого перехода к нужным данным'''
    ''' TODO: Поскольку данные упорядоченны по времени - то сохранять в индексе номер элемента с которого начинается заданное время'''
    return 0 unless @cache.demand
    return 0 unless @cache.demand.length
    time_point_top = new Date(time_point.getTime() + 900000)
    result_sum = 0
    demand_data_len=@cache.demand.length
    i = @current_positions.demand
    if not @current_positions.demand and @time_index.demand[time_point]
      @current_positions.demand = @time_index.demand[time_point]

    while i < demand_data_len
      demand_item = @cache.demand[i]
      demand_item.datetime = new Date(demand_item.datetime) unless demand_item.datetime instanceof Date
      continue if demand_item.datetime < time_point
      break if demand_item.datetime >= time_point_top
      @time_index.demand[demand_item.datetime] = i unless @time_index.demand[demand_item.datetime]
      ''' Здесь будет если только в интервале от time_point до time_point + 15 минут '''
      ''' TODO: добавить фильтрацию '''
      result_sum += demand_item.headcount
      i++
    @current_positions.demand = i
    result_sum

  chart_point_for_planned: (time_point)->
    # Функция аггрегирует данные "запланированно" по одной временной точке
    # Возвращает значение в этой точке
    '''TODO: Здесь индексация не поможет поскольку временные интервалы не отсортированы,
       поэтому нужно пройтись по всему массиву, а лучше придумать что-то более эффективное'''
    result_sum = 0
    return 0 unless @cache.planned
    return 0 unless @cache.planned.length
    for planned_item in @cache.planned
      for time_interval in planned_item.time_intervals
        for _time, i in time_interval
          time_interval[i] = new Date(_time) unless _time instanceof Date
        time_from = time_interval[0]
        time_to = time_interval[1]
        result_sum++ if time_point >= time_from and time_point < time_to
    result_sum


app.service 'wfmChartDataService', ChartDataService

