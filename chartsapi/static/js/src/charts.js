// Generated by CoffeeScript 1.8.0
(function() {
  var ChartDataService, app, param_date_formatter, test_data, tooltip_date_formatter;

  app = angular.module("wfm_charts", ['ui.bootstrap', 'ui.bootstrap.tpls', 'n3-line-chart']);

  test_data = function(x) {
    return {
      x: new Date(new Date().getTime() + x * 900000),
      demand: 100 * Math.sin(x * Math.PI / 18),
      planned: 100 * Math.cos(x * Math.PI / 18)
    };
  };

  app.controller('ChartCtrl', [
    '$scope', 'commonStorage', 'wfmChartDataService', function($scope, commonStorage, dataService) {
      var x;
      $scope.chart = commonStorage.chart;
      $scope.chart_type = 'line';
      window.dbg_cs = commonStorage;
      window.dbg_chart_scope = $scope;
      $scope.chart.data = dataService.get_chart_data('all');
      if (!$scope.chart.data) {
        $scope.chart.data = (function() {
          var _i, _results;
          _results = [];
          for (x = _i = 0; _i <= 36; x = ++_i) {
            _results.push(test_data(x));
          }
          return _results;
        })();
      }
      $scope.data_filters = {
        from: new Date("10 29 2015"),
        to: new Date("10 29 2015")
      };
      $scope.data_filters.to.setDate($scope.data_filters.from.getDate() + 7);
      $scope.chart.options.series = [$scope.chart.series_options.demand, $scope.chart.series_options.planned];
      $scope.reload_from_server = function() {
        return dataService.reload_from_server($scope.data_filters).then(function(d) {
          $scope.chart.data = dataService.get_chart_data('all', $scope.data_filters);
          return $scope.chart.options.series = [$scope.chart.series_options.demand, $scope.chart.series_options.planned];
        });
      };
      return $scope.$watch('chart_type', function() {
        var chrt, _i, _len, _ref, _results;
        _ref = $scope.chart.options.series;
        _results = [];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          chrt = _ref[_i];
          _results.push(chrt.type = $scope.chart_type);
        }
        return _results;
      });
    }
  ]);

  tooltip_date_formatter = d3.time.format('%m/%d %H:%M');

  param_date_formatter = d3.time.format('%Y-%m-%d %H:%M:%S');

  app.value("commonStorage", {
    config: {
      api_url: '/charts/api/working_times'
    },
    chart: {
      options: {
        axes: {
          x: {
            key: 'x',
            type: 'date',
            tooltipFormat: '%d.%m %M:%S',
            ticksFormat: '%H %d.%m',
            ticksRotate: 45,
            ticks: d3.time.hour,
            ticksInterval: 2,
            grid: true
          },
          y: {
            type: 'linear',
            label: 'Сотрудники',
            grid: true
          }
        },
        series: [],
        tooltip: {
          mode: 'scrubber',
          formatter: function(x, y, series) {
            return tooltip_date_formatter(x) + '  .   ' + (Math.round(y * 100) / 100);
          }
        },
        drawLegend: false,
        columnsHGap: 1,
        drawDots: false
      },
      data: [],
      series_options: {
        demand: {
          y: "demand",
          label: "Demand",
          color: "#5533FF",
          type: "line",
          thickness: '2px'
        },
        planned: {
          y: "planned",
          label: "Planned",
          color: "#ff3333",
          type: "line",
          thickness: '2px'
        }
      }
    }
  });

  ChartDataService = (function() {
    var chart_types;

    chart_types = ['demand', 'planned'];

    ChartDataService.$inject = ['$q', '$http', 'commonStorage'];

    function ChartDataService($q, $http, commonStorage) {
      var ct, _i, _len;
      this.$q = $q;
      this.$http = $http;
      this.commonStorage = commonStorage;
      this.url = this.commonStorage.config.api_url;
      this.cache = {};
      this.time_index = {};
      this.current_positions = {};
      for (_i = 0, _len = chart_types.length; _i < _len; _i++) {
        ct = chart_types[_i];
        this.time_index[ct] = {};
        this.current_positions[ct] = 0;
        this.cache[ct] = JSON.parse(localStorage.getItem(ct));
      }
    }

    ChartDataService.prototype.reload_from_server = function(filters) {
      var self;
      if (filters == null) {
        filters = null;
      }
      self = this;
      return this.$http({
        url: this.url,
        method: 'POST',
        responseType: "json",
        xsrfCookieName: "csrftoken",
        xsrfHeaderName: "X-CSRFToken",
        data: filters
      }).then(function(resp) {
        'TODO: чтобы не делать много преобразований со времением - можно данные с сервера получать в виде integer=timestamp';
        var data, k, _ref;
        _ref = resp.data;
        for (k in _ref) {
          data = _ref[k];
          'TODO: проверять есть ли уже такой ключ и консалидировать данные с теми что уже есть';
          localStorage.setItem(k, JSON.stringify(data));
        }
        return resp.data;
      }, function(resp) {
        return console.log('Ajax error', resp);
      });
    };

    ChartDataService.prototype.get_chart_data = function(chart_type, filters) {
      var count_x_point, ct, n, start_time_point_ms, _i, _j, _len, _len1;
      if (chart_type == null) {
        chart_type = 'demand';
      }
      if (filters == null) {
        filters = null;
      }
      ' TODO: Определить в каких еще случаях нужно считать данные из хранилища ';
      if (chart_type === 'all') {
        for (_i = 0, _len = chart_types.length; _i < _len; _i++) {
          ct = chart_types[_i];
          if (!this.cache[ct]) {
            this.cache[ct] = JSON.parse(localStorage.getItem(ct));
          }
        }
      }
      if (!this.cache[chart_type]) {
        this.cache[chart_type] = JSON.parse(localStorage.getItem(chart_type));
      }
      if (!filters) {
        filters = {
          from: new Date(new Date().toDateString()),
          to: new Date(new Date().toDateString())
        };
        filters.to.setDate(filters.from.getDate() + 7);
      }
      start_time_point_ms = filters.from.getTime();
      count_x_point = (filters.to.getTime() - start_time_point_ms) / 900000;
      if (chart_type === 'all') {
        for (_j = 0, _len1 = chart_types.length; _j < _len1; _j++) {
          ct = chart_types[_j];
          this.current_positions[ct] = 0;
        }
      } else {
        this.current_positions[chart_type] = 0;
      }
      if (chart_type !== 'all' && !this.cache[chart_type]) {
        return [];
      }
      return (function() {
        var _k, _results;
        _results = [];
        for (n = _k = 0; _k <= count_x_point; n = _k += 1) {
          _results.push(this.make_timepoint(new Date(start_time_point_ms + 900000 * n), chart_type));
        }
        return _results;
      }).call(this);
    };

    ChartDataService.prototype.make_timepoint = function(time_point, chart_type) {
      ' Функция создает точку на графиках. chart_type - тип гарфика или all если оба\nточнее один элемент массива для компоненты вывода графика';
      var ct, item, _i, _len;
      item = {
        x: time_point
      };
      if (chart_type === 'all') {
        for (_i = 0, _len = chart_types.length; _i < _len; _i++) {
          ct = chart_types[_i];
          item[ct] = this.make_timepoint(time_point, ct)[ct];
        }
      } else {
        item[chart_type] = this["chart_point_for_" + chart_type](time_point);
      }
      return item;
    };

    ChartDataService.prototype.chart_point_for_demand = function(time_point) {
      ' TODO: формировать и использовать индекс для быстрого перехода к нужным данным';
      ' TODO: Поскольку данные упорядоченны по времени - то сохранять в индексе номер элемента с которого начинается заданное время';
      var demand_data_len, demand_item, i, result_sum, time_point_top;
      if (!this.cache.demand) {
        return 0;
      }
      if (!this.cache.demand.length) {
        return 0;
      }
      time_point_top = new Date(time_point.getTime() + 900000);
      result_sum = 0;
      demand_data_len = this.cache.demand.length;
      i = this.current_positions.demand;
      if (!this.current_positions.demand && this.time_index.demand[time_point]) {
        this.current_positions.demand = this.time_index.demand[time_point];
      }
      while (i < demand_data_len) {
        demand_item = this.cache.demand[i];
        if (!(demand_item.datetime instanceof Date)) {
          demand_item.datetime = new Date(demand_item.datetime);
        }
        if (demand_item.datetime < time_point) {
          continue;
        }
        if (demand_item.datetime >= time_point_top) {
          break;
        }
        if (!this.time_index.demand[demand_item.datetime]) {
          this.time_index.demand[demand_item.datetime] = i;
        }
        ' Здесь будет если только в интервале от time_point до time_point + 15 минут ';
        ' TODO: добавить фильтрацию ';
        result_sum += demand_item.headcount;
        i++;
      }
      this.current_positions.demand = i;
      return result_sum;
    };

    ChartDataService.prototype.chart_point_for_planned = function(time_point) {
      'TODO: Здесь индексация не поможет поскольку временные интервалы не отсортированы,\nпоэтому нужно пройтись по всему массиву, а лучше придумать что-то более эффективное';
      var i, planned_item, result_sum, time_from, time_interval, time_to, _i, _j, _k, _len, _len1, _len2, _ref, _ref1, _time;
      result_sum = 0;
      if (!this.cache.planned) {
        return 0;
      }
      if (!this.cache.planned.length) {
        return 0;
      }
      _ref = this.cache.planned;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        planned_item = _ref[_i];
        _ref1 = planned_item.time_intervals;
        for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
          time_interval = _ref1[_j];
          for (i = _k = 0, _len2 = time_interval.length; _k < _len2; i = ++_k) {
            _time = time_interval[i];
            if (!(_time instanceof Date)) {
              time_interval[i] = new Date(_time);
            }
          }
          time_from = time_interval[0];
          time_to = time_interval[1];
          if (time_point >= time_from && time_point < time_to) {
            result_sum++;
          }
        }
      }
      return result_sum;
    };

    return ChartDataService;

  })();

  app.service('wfmChartDataService', ChartDataService);

}).call(this);

//# sourceMappingURL=charts.js.map
